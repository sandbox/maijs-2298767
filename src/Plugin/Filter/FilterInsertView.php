<?php

/**
 * @file
 * Contains \Drupal\filter\Plugin\Filter\FilterInsertView.
 */

namespace Drupal\insert_view\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\views\Views;

/**
 * Provides a filter which allows to embed views.
 *
 * @Filter(
 *   id = "insert_view",
 *   title = @Translation("Embed views in text"),
 *   description = @Translation("Allows embedding views displays in text."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class FilterInsertView extends FilterBase {
  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    if (preg_match_all("/\[view:([^=\]]+)=?([^=\]]+)?=?([^\]]*)?\]/i", $text, $match)) {
      foreach ($match[0] as $key => $value) {
        $view_name = $match[1][$key];
        $display_id = ($match[2][$key] && !is_numeric($match[2][$key])) ? $match[2][$key] : 'default';
        $args = $match[3][$key];

        $view_output = $this->insertView($view_name, $display_id, $args);

        $search[] = $value;
        $replace[] = !empty($view_output) ? $view_output : '';
      }
      $text = str_replace($search, $replace, $text);
    }

    return new FilterProcessResult($text);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    // General help string.
    $output = $this->t('You may use the token @insert_view_tag_format to display views.', array('@insert_view_tag_format' => '[view:name=display=args]'));

    // Return if short help is requested.
    if (!$long) {
      return $output;
    }

    $output = '<p>' . $output . '</p>';
    $t_args = array('@insert_view_tag_format' => '[view:tracker=page=1]', '%view_name' => 'tracker', '%display_name' => 'page', '@argument' => 1);
    $output .= '<p>' . $this->t('For example, a tag @insert_view_tag_format will embed a view named %view_name, use the %display_name display and supply the argument @argument to it.', $t_args) . '</p>';

    $output .= '<p>' . $this->t("The <em>display</em> and <em>args</em> parameters can be omitted. If the display is left empty, the view's default display is used.") . '</p>';
    $output .= '<p>' . $this->t('Multiple arguments are separated with slash. The <em>args</em> format is the same as used in the URL (or view preview screen).') . '</p>';
    
    $examples = 'Valid examples:
        <dl>
        <dt>[view:my_view]</dt>
        <dt>[view:my_view=my_display]</dt>
        <dt>[view:my_view=my_display=arg1/arg2/arg3]</dt>
        <dt>[view:my_view==arg1/arg2/arg3]</dt>';
    $output .= '<p>' . $this->t($examples) . '</p>';
    
    return $output;
  }

  /**
   * Embed a view using a PHP snippet.
   *
   * This function is meant to be called from PHP snippets, should one wish to
   * embed a view in a node or something. Other than embedding the view it
   * checks the view access and also allows to use view arguments grabbed from
   * the URL.
   *
   * @param string $view_name
   *   The name of the view.
   * @param string $display_id
   *   The display id to embed. If unsure, use 'default', as it will always be
   *   valid. But things like 'page' or 'block' should work here.
   * @param string $args
   *   Additional arguments to send to the view as if they were part of the URL
   *   in the form of arg1/arg2/arg3. You may use %0, %1, ..., %N to grab
   *   arguments from the URL.
   *
   * @return string
   *   A string with the view output.
   */
  public function insertView($view_name, $display_id = 'default', $args = '') {
    if (empty($view_name)) {
      return;
    }

    // Get view.
    $view = Views::getView($view_name);
    if (empty($view)) {
      return;
    }

    // Check access.
    if (!$view->access($display_id)) {
      return;
    }

    // Override the view's path to the current path. Otherwise, exposed
    // views filters would submit to the front page.
    $view->override_path = current_path();

    $url_args = explode('/', current_path());
    foreach ($url_args as $id => $arg) {
      $args = str_replace("%$id", $arg, $args);
    }
    $args = preg_replace(',/?(%\d),', '', $args);
    $args = $args ? explode('/', $args) : array();

    $view->setDisplay($display_id);
    $view->setArguments($args);
    $output = $view->preview($display_id, $args);
    $output = drupal_render($output);
    $view->destroy();

    return $output;
  }
}
